package ap.core

case class Message(messageType: MessageType.Value,
                   contents: String = "",
                   username: String = "",
                   destination: String = "Server") {
  def printFormatted() = {
    val delimiter =
      if (destination == "Server")
        " $ "
      else if (messageType == MessageType.Private)
        " ! "
      else
        " @ "
    println(username + delimiter + destination + ": " + contents)
  }
}
