package ap.core

import java.net.{Socket, ServerSocket}
import java.net.InetAddress
import java.net.BindException
import java.net.SocketException
import java.io.ObjectOutputStream
import java.io.ObjectInputStream
import java.io.EOFException

object Server extends App {
  checkArgs(args)
  val Hostname = args(0)
  val Port = Integer.parseInt(args(1))
  val ServerSocket = initializeSocket(Port, Hostname)

  // Messages
  val StartupErrorMsg = "Run with <Hostname, Port>"
  val StartupMsg = "Server started up successfully"
  val BindErrorMsg = "Address already in use. Exiting"
  val ConnectionAcceptedMsg = "Accepted new connection from "
  val ReceivedNewServerMsg = "Server received the following message: "
  val PromptMessage =
    "Enter a username.\nIt must not contain spaces or start with '/': "
  val ConfirmationMsg = "You are now known as "
  val TypeErrorMsg = "Received object is not an instance of class Message"
  val InvalidMsgErrorMsg = "Received message has invalid type"
  val UserExitMsg = " has disconnected"
  val MustJoinRoomMessage = "You must first join a room to send a message"
  val MissingArgumentMsg = "Command needs an argument"
  val QuitConfirmationMsg = "Quit room "
  val CannotQuitRoomMsg = "Cannot quit room "

  var roomUsersMap = Map[String, Set[String]]() // maps rooms to its set
                                                     // of connected users

  var userRoomsMap= Map[String, Set[String]]() // maps users to their set
                                                     // of connected rooms

  var userStreamMap = Map[String, ObjectOutputStream]() // maps users to their
                                                        // corresponding output
                                                        // stream
  
  println(StartupMsg)
  /*
   * Accept connections, starting a new thread for each user
   * This is the main thread of the server
   */
  while (true) {
    val clientSocket = ServerSocket.accept()
    val clientAddr = clientSocket.getInetAddress().getHostAddress()
    val newThread = new Thread(new ServerSessionThread(clientSocket))
    println(ConnectionAcceptedMsg + clientAddr)
    newThread.start()
  }

  private def checkArgs(args: Array[String]) {
    if (args.size != 2) {
      System.err.print(StartupErrorMsg)
      System.exit(1)
    }
  }

  private def initializeSocket(Port: Integer, host: String): ServerSocket = {
    try {
      new ServerSocket(Port, 100, InetAddress.getByName(host))
    } catch {
      case e: BindException =>
        System.err.print(BindErrorMsg)
        sys.exit(1)
    }
  }

  private class ServerSessionThread(clientSocket: Socket) extends Runnable {
    val outputStream = new ObjectOutputStream(clientSocket.getOutputStream())
    val inputStream = new ObjectInputStream(clientSocket.getInputStream())
    var userConnected = true

    /*
     * Thread main method, run for each newly created thread.
     */
    def run() = {
      var username = ""
      try {
        username = obtainUsername()
        while (userConnected) {
          val receivedMessage = receiveMessage()
          println(ReceivedNewServerMsg + '\n' + receivedMessage)

          receivedMessage.messageType match {
            case MessageType.Room =>
              processRoomMessage(receivedMessage)
            case MessageType.JoinChatRoom =>
              processJoinChatRoomMessage(receivedMessage)
            case MessageType.Query =>
              processQueryMessage(receivedMessage)
            case MessageType.Private =>
              processPrivateMessage(receivedMessage)
            case MessageType.QuitChatRoom =>
              processQuitChatRoomMessage(receivedMessage)
            case MessageType.Exit =>
              processExitMessage(receivedMessage)
            case _ => throw new Exception(InvalidMsgErrorMsg)
          }
        }
      } catch {
        case e @ (_: SocketException | _: EOFException) =>
          disconnectUser(username)
      }
    }

    /*
     * Prompts the newly connected user for the username.
     * Usernames may not start with '/' or contain spaces.
     * If the requested username already exists on the server,
     * the character '2' is appended to it.
     *
     * Returns the required username.
     */
    def obtainUsername(): String = {
      var validUsernameObtained = false
      var requestedUsername = ""

      while (!validUsernameObtained) {
        sendControlMessage(MessageType.UsernamePrompt, PromptMessage)
        val usernameRequestMessage = receiveMessage()
        requestedUsername = usernameRequestMessage.contents
        if (!requestedUsername.startsWith("/") && !requestedUsername.contains(' '))
          validUsernameObtained = true
      }
      while (userRoomsMap.contains(requestedUsername))
        requestedUsername += '2'
      userRoomsMap += requestedUsername -> Set()
      userStreamMap += requestedUsername -> outputStream
      val confirmationMessage = Message(MessageType.UsernameConfirmation,
                        ConfirmationMsg + requestedUsername)
      sendMessageToUser(confirmationMessage, requestedUsername)
      requestedUsername
    }

    /*
     * Sends a control message of type messageType to the
     * user "controlled" by the current thread."
     */
    def sendControlMessage(messageType: MessageType.Value, contents: String = "") = {
      val message = new Message(messageType, contents)
      outputStream.writeObject(message)
    }

    /*
     * Sends a non-control message to a certain user.
     */
    def sendMessageToUser(message: Message, user: String) = {
      val UserOutputStream = userStreamMap(user)
      UserOutputStream.writeObject(message)
      println("Sent message to " + user)
    }

    /*
     * Reads a Message object from the input stream and returns it.
     */
    def receiveMessage(): Message = {
      inputStream.readObject() match {
        case msg: Message => msg
        case _ => throw new Exception(TypeErrorMsg)
      }
    }

    /*
     * Scans a normal Room message and sends it to all
     * users in the chatroom, except the sender.
     */
    def processRoomMessage(message: Message) = {
      val sender = message.username
      val destinationRoom = message.destination
      if (destinationRoom == "") {
        sendControlMessage(MessageType.MustJoinChatRoom,
          MustJoinRoomMessage)
      } else {
        for (user <- roomUsersMap(destinationRoom) - sender)
          sendMessageToUser(message, user)
      }
    }

    /*
     * Adds the sender to the set of users currently connected
     * to a chatroom, creating it if it doesn't yet exist.
     *
     * All other users in the room are informed of the new user.
     */
    def processJoinChatRoomMessage(message: Message) = {
      val sender = message.username
      val roomToJoin = message.destination
      val userRoomsSet = userRoomsMap(sender)

      if (roomToJoin == "") {
        sendControlMessage(MessageType.MissingArgument, MissingArgumentMsg)
      } else {
        val roomUsersSet: Set[String] =
          if (roomUsersMap.contains(roomToJoin))
            roomUsersMap(roomToJoin)
          else
            Set()
        userRoomsMap += sender -> (userRoomsSet + roomToJoin)
        roomUsersMap += roomToJoin -> (roomUsersSet + sender)

        println("User " + sender + " joined room " + roomToJoin)
        val JoinUpdateMsg = new Message(
          MessageType.Room,
          "User " + sender + " joined room " + roomToJoin,
          sender,
          roomToJoin)
        val JoinConfirmationMsg = new Message(
          MessageType.JoinConfirmation,
          "joined room " + roomToJoin,
          "", roomToJoin)
        sendMessageToUser(JoinConfirmationMsg, sender)
        processRoomMessage(JoinUpdateMsg) //sends message to other users
      }
    }

    /*
     * Processes a query message, checking if the desired user
     * exists on the server. The outcome is sent to the querying user.
     */
    def processQueryMessage(message: Message) = {
      val sender = message.username
      val userToQuery = message.destination
      if (userToQuery == "") {
        sendControlMessage(MessageType.MissingArgument, MissingArgumentMsg)
      } else {
        val responseMessage =
          if (userRoomsMap.contains(userToQuery))
            new Message(MessageType.QueryConfirmation,
                        "Querying user " + userToQuery, "", userToQuery)
          else
            new Message(MessageType.QueryError,
                        "User " + userToQuery + " does not exist on this server")
        sendMessageToUser(responseMessage, sender)
      }
    }

    /*
     * Checks whether the queried user still exists on the server. If it
     * does, sends a private message to it. Otherwise, the sender is
     * informed.
     */
    def processPrivateMessage(message: Message) = {
      val sender = message.username
      val userToQuery = message.destination
      val isDestinationUserActive = userRoomsMap.contains(userToQuery)
      val responseMessage =
        if (isDestinationUserActive)
          message
        else
          new Message(MessageType.QueryCutoff,
            "User " + userToQuery + " has disconnected during query")
      val destination =
        if (isDestinationUserActive) userToQuery else sender
      sendMessageToUser(responseMessage, destination)
    }

    /*
     * Removes the sender from the users currently connected to the
     * chatroom
     */
    def processQuitChatRoomMessage(message: Message) = {
      val quittingUser = message.username
      val destinationRoom = message.destination
      val userRoomsSet = userRoomsMap(quittingUser)

      if (destinationRoom == ""
          || !roomUsersMap.contains(destinationRoom)
          || !roomUsersMap(destinationRoom).contains(quittingUser)) {
        val(failureMessage, failureMessageType) =
          if (destinationRoom == "")
            (MissingArgumentMsg, MessageType.MissingArgument)
          else
            (CannotQuitRoomMsg + destinationRoom, MessageType.NonexistentRoom)
        sendControlMessage(failureMessageType, failureMessage)
      } else {
        userRoomsMap += quittingUser -> (userRoomsSet - destinationRoom)
        roomUsersMap += destinationRoom -> (roomUsersMap(destinationRoom) - quittingUser)
        val userQuitMessage =
          new Message(MessageType.Room,
            "User " + quittingUser + " has quit",
            quittingUser,
            destinationRoom)
        processRoomMessage(userQuitMessage)
        val quitConfirmationMessage =
          new Message(MessageType.QuitConfirmation,
            QuitConfirmationMsg, "", destinationRoom)
        sendMessageToUser(quitConfirmationMessage, quittingUser)
      }
    }

    /*
     * Informs all users connected to all chatrooms the exiting user
     * was connected to, then disconnects the user.
     */
    def processExitMessage(message: Message) = {
      val exitingUser = message.username

      val exitConfirmationMessage = 
        new Message(MessageType.ExitConfirmation)
      sendMessageToUser(exitConfirmationMessage, exitingUser)
      disconnectUser(exitingUser)
    }

    /*
     * Closes streams, removes user from hashmaps and
     * closes connection to the user
     */
    def disconnectUser(exitingUser: String) {
      if (!userRoomsMap.keySet.contains(exitingUser)) {
        println("User disconnected before receiving username :(")
        println("Client from " + clientSocket
            .getInetAddress()
            .getHostAddress() + " disconnected")
      } else {
        for (room <- userRoomsMap(exitingUser)) {
          for (user <- roomUsersMap(room) - exitingUser) {
            val exitNotificationMessage =
              new Message(MessageType.ExitNotification,
                exitingUser + UserExitMsg)
            sendMessageToUser(exitNotificationMessage, user)
          }
          roomUsersMap += room -> (roomUsersMap(room) - exitingUser)
        }
        userStreamMap -= exitingUser
        userRoomsMap -= exitingUser
        println("User " + exitingUser + " has disconnected")
        println("Client from " + clientSocket
            .getInetAddress()
            .getHostAddress() + " disconnected")
      }
      inputStream.close()
      outputStream.close()
      clientSocket.close()
      userConnected = false
    }
  }
}
