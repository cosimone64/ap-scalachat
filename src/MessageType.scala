package ap.core

object MessageType extends Enumeration {
  val Room = Value
  val Query = Value
  val QueryConfirmation = Value
  val QueryError = Value
  val QueryCutoff = Value
  val Private = Value
  val JoinChatRoom = Value
  val MustJoinChatRoom = Value
  val JoinConfirmation = Value
  val MissingArgument = Value
  val NonexistentRoom = Value
  val QuitChatRoom = Value
  val QuitConfirmation = Value
  val Exit = Value
  val ExitNotification = Value
  val ExitConfirmation = Value
  val UsernameConfirmation = Value
  val UsernamePrompt = Value
  val UsernameRequest = Value
}
