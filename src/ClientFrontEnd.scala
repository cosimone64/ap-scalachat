package ap.cli

import ap.core.Client
import ap.core.Message
import ap.core.MessageType

import scala.io.StdIn

object ClientFrontEnd extends App {
  // Messages
  val StartupErrorMsg = "Run with <Hostname, Port>"
  val InvalidMessageTypeError = "Invalid message type"
  val StandardPrompt = "> "
  val CommandLineStartPrompt = "Launching client in command line mode"

  // Allows the application terminate gracefully on Control+C
  Client.checkArgs(args)
  Runtime
    .getRuntime()
    .addShutdownHook(new Thread(new Runnable {
      def run() =
        if (Client.connected) {
          Client.connected = false
          Client.disconnect()
        }
    }))

  val Hostname = args(0)
  val Port = Integer.parseInt(args(1))
  val SuccessfulConnectionMsg = "Connected to server at " + Hostname + ": " + Port
  val Listener = new Thread(new ListenerThread)

  Client.connectSocket(Hostname, Port)
  Client.startListenerThread()
  Listener.start()
  println(CommandLineStartPrompt)

  //Main loop
  while (Client.connected) {
    val RawMessage = StdIn.readLine()
    if (RawMessage != "") {
      try {
        val NewMessage =
          if (Client.usernameObtained)
            createMessage(RawMessage)
          else
            new Message(MessageType.UsernameRequest, RawMessage)
        Client.sendMessageToServer(NewMessage)
      } catch {
        case e: IllegalStateException => {
          println(InvalidMessageTypeError)
        }
      }
    }
    print(StandardPrompt)
  }
  Client.disconnect()

  /*
   * Parses raw input and returns an appropriate Message object
   */
  def createMessage(rawMessage: String): Message = {
    val NewMessageType =
      if (!rawMessage.startsWith("/") && !Client.isQueryOngoing)
        MessageType.Room
      else if (!rawMessage.startsWith("/"))
        MessageType.Private
      else if (rawMessage.startsWith("/join"))
        MessageType.JoinChatRoom
      else if (rawMessage.startsWith("/query"))
        MessageType.Query
      else if (rawMessage.startsWith("/quit"))
        MessageType.QuitChatRoom
      else if (rawMessage.startsWith("/exit"))
        MessageType.Exit
      else
        throw new IllegalStateException()

    val destination =
      if (Set(MessageType.JoinChatRoom,
              MessageType.Query,
              MessageType.QuitChatRoom).contains(NewMessageType)) {
        try {
          rawMessage.split(' ')(1)
	} catch {
          case e: ArrayIndexOutOfBoundsException => ""
        }
      } else {
        Client.currentDestination
      }

    new Message(NewMessageType, rawMessage, Client.username, destination)
  }

  private class ListenerThread extends Runnable {
    /*
     * Prints all messages currently in the lists owned by Client.
     */
    def run() = {
      while (Client.connected) {
        Thread.sleep(1000)
        val messageLists = Client.getAndResetMessageLists()
        // if there are no new messages to print, print the prompt to
        // the user
        val Prompt = if (messageLists.isEmpty) "" else "> "
        for (chatRoom <- messageLists.keys)
          for (message <- messageLists(chatRoom))
            message.printFormatted()
        print(Prompt)
      }
    }
  }
}
