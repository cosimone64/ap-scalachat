package ap.core

import scala.io.StdIn
import java.net.Socket
import scala.collection.mutable.ListBuffer
import java.net.InetAddress
import java.net.BindException
import java.net.ConnectException
import java.io.ObjectOutputStream
import java.io.ObjectInputStream

object Client {
  // Messages
  val StartupErrorMsg = "Run with <Hostname, Port>"
  val TypeErrorMsg = "Received object is not an instance of class Message"
  val ConnectErrorMsg = "An error occurred while connecting to server"
  val ConfirmedUsernameMsg = "Logged in with username"
  val NoChatRoomErrorMsg = "You must first join a chatroom to send a message"
  val InvalidMessageTypeError = "Invalid message type"
  val AlreadyJoinedRoomMsg = "You are alredy in this room"
  val JoinedNewRoomMsg = "Joined room "
  val AlreadyQuitRoomMsg = "You are not in this room"
  val CannotChangeToRoomMsg = "Cannot change to a room you have not joined"
  val Listener = new Thread(new ListenerThread)
  val MessageListLock = new Object

  private var socket: Socket = null
  private var outputStream: ObjectOutputStream = null
  private var inputStream: ObjectInputStream = null
  private var messageLists: Map[String, ListBuffer[Message]] = Map()
  private var joinedChatRooms: Set[String] = Set()
  var currentDestination = ""
  var username = ""
  var hostname = ""
  var port = ""
  var usernameObtained = false
  var isQueryOngoing = false
  var connected = false

  /*
   * Checks if the correct number of arguments is provided.
   */
  def checkArgs(args: Array[String]) {
    if (args.size != 2) {
      System.err.print(StartupErrorMsg)
      sys.exit(1)
    }
  }

  def startListenerThread() =
    Listener.start()

  def sendMessageToServer(message: Message) =
    outputStream.writeObject(message)

  /*
   * Returns a copy of the message list hash table and
   * reinitializes it to an empty map, so that the front end
   * can process it.
   */
  def getAndResetMessageLists() = synchronized {
    val messageListsAux = messageLists
    messageLists = Map()
    messageListsAux
  }

  /*
   * Initializes the Socket and connects it to a server
   * located at (hostname, port).
   * Initializes input and output object streams.
   */
  def connectSocket(hostname: String, port: Int) = {
    try {
      socket = new Socket(InetAddress.getByName(hostname), port)
      outputStream = new ObjectOutputStream(socket.getOutputStream())
      inputStream = new ObjectInputStream(socket.getInputStream())
      connected = true
    } catch {
      case e: ConnectException =>
        System.err.println(ConnectErrorMsg)
        sys.exit(1)
    }
  }

  /*
   * Disconnects the client from the server, closing all streams and
   * the socket.
   */
  def disconnect() = {
    inputStream.close()
    outputStream.close()
    socket.close()
  }

  private class ListenerThread extends Runnable {
    /*
     * This listener STORES messages in the queues,
     * the frontend's listener periodically (1s) checks the queue
     * and prints messages.
     */
    def run() = {
      while (connected) {
        val receivedMessage = receiveMessage()
        processIncomingMessage(receivedMessage)
        storeReceivedMessage(receivedMessage)
      }
    }

    /*
     * Receives a message from the input stream and returns it.
     */
    def receiveMessage(): Message = {
      inputStream.readObject() match {
        case msg: Message => msg
        case _ => throw new Exception(TypeErrorMsg)
      }
    }

    /*
     * Scans the newly received message and performs the necessary
     * operations if they are needed.
     */
    def processIncomingMessage(message: Message) = {
      message.messageType match {
        case MessageType.UsernameConfirmation =>
          usernameObtained = true
          username = message.contents.split(' ').last
        case MessageType.JoinConfirmation =>
          currentDestination = message.destination
          isQueryOngoing = false
        case MessageType.QueryConfirmation =>
          currentDestination = message.destination
          isQueryOngoing = true
        case MessageType.QueryCutoff =>
          currentDestination = ""
          isQueryOngoing = false
        case MessageType.QuitConfirmation =>
          if (message.destination == currentDestination)
            currentDestination = ""
        case MessageType.ExitConfirmation =>
          connected = false
          System.exit(0)
        case _ => ;
      }
    }

    /*
     * Stores the newly received message in its respective list in the
     * message list hash table.
     */
    def storeReceivedMessage(message: Message) = synchronized {
      val destination = message.destination
      if (!messageLists.contains(destination))
        messageLists += destination -> ListBuffer()
      messageLists(destination) += message
    }
  }
}
