SRC=src/
all:
	scalac $(SRC)Message.scala $(SRC)ClientFrontEnd.scala $(SRC)MessageType.scala\
		$(SRC)Server.scala $(SRC)Client.scala
	printf '#!/usr/bin/env /bin/bash\nscala ap.core.Server localhost 10000' > server
	printf '#!/usr/bin/env /bin/bash\nscala ap.cli.ClientFrontEnd localhost 10000' > cli
	chmod 744 server
	chmod 744 cli
clean:
	rm -r ap server cli
